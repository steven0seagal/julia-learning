# string interpolation

my_string = "Hello World!"

println("Pete says: $(my_string)")

println("Pete says: $my_string")

println("Pete says: ", my_string)


n = 42
println("Pete says: $(n)")
m = 3.14

println("Pete says: $(m * n)")

println("Pete says: $(m * n) is not $(n * m)")