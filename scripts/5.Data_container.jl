#string indexing
# there is no indexing from 0 in Julia
my_string = "Hello World!"

println(my_string[1]) # first character

println(my_string[1:2]) # first two characters

println(my_string[1:3:10]) # every third character from 1 to 10

println(my_string[1:3:end]) # every third character from 1 to end

init = 1
fin = 3

println(my_string[init:fin]) # first three characters

my_range = init:fin

println(my_string[my_range]) # first three characters

println(my_string[end:-1:1]) # reverse string

# string length

println(length(my_string))

println(ncodeunits(my_string))

# char looping

for c in my_string
    println(c)
end

list_of_chars = [x for x in my_string]

println(list_of_chars)