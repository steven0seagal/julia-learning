# ready string function

a = string("Hello", " ", "World!", 69)
println(a)

b = string(123)
println(b)

# join function

strarray = ["a", "b", "c", "d", "e"]
println(join(strarray, ", "))

# split function

println(split("a,b,c,d,e", ","))


