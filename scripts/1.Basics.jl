# Strings

my_string = "Hello World!"
println(my_string)

my_float = 3.14
println(my_float)

my_int = 42
println(my_int)

bool_true = true
println(bool_true)

bool_false = false
println(bool_false)

