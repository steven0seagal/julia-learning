# operator * examples

println("Hello" * " " * "World")

println("Hello" * " " * "World" * "!")

println("Hello" * " " * "World" * "!" * "!" * "!")

# operator ^ examples

println("Hello"^3)

println("Hello" * " " * "World"^3 * "!" * "!" * "!")