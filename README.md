# Julia-learning
this repository is clearly for julia learning purposes


## Conten of scripts directory

1. Basics.jl - basic objects 
2. Include.jl - how to import code from other file
3. String_literals.jl - operators * and ^
4. String_interpolation.jl - f string for julia 
5. Data_container.jl - messing around with string 
6. Functions.jl - first functions
## Useful commands

1. Running file
```bash
julia file.jl
```
2. Get information about function
```julia
?string
```
